cmake_minimum_required(VERSION 3.14)

project(tg-notifier)

set(CMAKE_CXX_STANDARD 20)

file(GLOB_RECURSE SOURCES_FILES src/*)

add_compile_definitions(TOKEN=\"$ENV{TOKEN}\")
add_compile_definitions(CHAT_ID=$ENV{CHAT_ID})

add_executable(tg-notifier ${SOURCES_FILES})
target_include_directories(tg-notifier PRIVATE ${CMAKE_CURRENT_SOURCE_DIR}/src)

target_link_libraries(tg-notifier TgBot)

find_package(Threads REQUIRED)
find_package(OpenSSL REQUIRED)
find_package(Boost COMPONENTS system REQUIRED)
find_package(CURL)
 
include(FetchContent)
FetchContent_Declare(
  TgBot
  GIT_REPOSITORY https://github.com/reo7sp/tgbot-cpp.git
  GIT_TAG        origin/master
)
FetchContent_MakeAvailable(TgBot)

