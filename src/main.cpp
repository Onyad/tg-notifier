#include <csignal>
#include <cstdio>
#include <cstdlib>
#include <exception>
#include <string>
#include <iostream>

#include <tgbot/tgbot.h>

using namespace std;
using namespace TgBot;

int main(int argc, const char* argv[]) {
    string token(TOKEN);
    string result;

    for (size_t i = 1; i < argc; ++i) {
            result += string(argv[i]) + (i + 1 == argc ? "" : " ");
    }

    if (result.empty()) {
        result = "Notify";
    }

    Bot bot(token);
    bot.getApi().sendMessage(CHAT_ID, result);

    return 0;
}
